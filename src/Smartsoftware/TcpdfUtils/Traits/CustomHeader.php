<?php namespace Smartsoftware\TcpdfUtils\Traits;

trait CustomHeader {

    protected $header_rederer = null;

     //Page header
    public function Header() {
        $c = $this->header_rederer;
        if (!is_callable($c)) {
            throw new Exception("header_rederer must be callable");
        }
        $c($this);
    }

    public function setHeaderRenderer($callback)
    {
        $this->header_rederer = $callback;
    }
}