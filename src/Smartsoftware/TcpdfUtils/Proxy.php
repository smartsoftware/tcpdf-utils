<?php namespace Smartsoftware\TcpdfUtils;

use TCPDF;

class Proxy {
    public $fluent;

    function __construct(Fluent $child) {
        $this->fluent = $child;
    }

    function __call($method, $args) {

        return call_user_func_array(array($this->fluent, $method), $args);

    }

    public function __get($name) {
        return $this->fluent->$name;
    }

    public function __set($name, $value) {
        $this->fluent->$name = $value;
    }
}