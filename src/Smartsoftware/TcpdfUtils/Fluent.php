<?php namespace Smartsoftware\TcpdfUtils;


class Fluent
{
    public $pdf;

    protected $current_font;
    protected $current_color;
    protected $height = 0;
    protected $width  = 50;
    protected $border = false;
    protected $filled = false;
    protected $align  = 'L';

    protected $x_offset = 0;
    protected $y_offset = 0;

    function __construct($pdf) {
        $this->pdf = $pdf;
    }

    public function write($text)
    {
        $this->pdf->Write($this->height, $text, '', $this->filled, $this->align, false, 0, false, false, 0);
        return $this;
    }

    public function writeLn($text)
    {
        $this->pdf->Write($this->height, $text, '', $this->filled, $this->align, true, 0, false, false, 0);
        return $this;
    }

    public function border($border) {
        $this->border = $border;
        return $this;
    }

    public function color($r, $g = null, $b = null)
    {
        if (is_array($r)) list($r, $g ,$b) =  $r;

        if ($g && $b) {
            $this->pdf->SetTextColor($r, $g, $b);
        } else {
            $this->pdf->SetTextColor($r);
        }
        return $this;
    }

    public function fillColor($r, $g = null, $b = null)
    {
        if (is_array($r)) list($r, $g ,$b) =  $r;

        if ($g && $b) {
            $this->pdf->setFillColor($r, $g, $b);
        } else {
            $this->pdf->setFillColor($r);
        }
        return $this;
    }

    public function drawColor($r, $g = null, $b = null)
    {
        if (is_array($r)) list($r, $g ,$b) =  $r;

        if ($g && $b) {
            $this->pdf->SetDrawColor($r, $g, $b);
        } else {
            $this->pdf->SetDrawColor($r);
        }
        return $this;
    }

    public function filled($f) {
        $this->filled = $f;
        return $this;
    }

    public function align($a='L') {
        $this->align = $a;
        return $this;
    }

    public function width($w) {
        $this->width = $w;
        return $this;
    }

    public function height($h) {
        $this->height = $h;
        return $this;
    }

    public function cell($text)
    {
        $this->pdf->Cell($this->width, $this->height, $text, $this->border, 0, $this->align, $this->filled);
        return $this;
    }

    public function cellLn($text)
    {
        $this->pdf->Cell($this->width, $this->height, $text, $this->border, 1, $this->align, $this->filled);
        return $this;
    }

    public function ln($h=null)
    {
        $this->pdf->Ln($h);
        return $this;
    }

    public function addPage($orientation='', $format='', $keepmargins=false, $tocpage=false)
    {
        $this->pdf->AddPage($orientation, $format, $keepmargins, $tocpage);
        return $this;
    }

    public function setY($y)
    {
        $this->pdf->setY($y+$this->y_offset);
        return $this;
    }

    public function setX($x)
    {
        $this->pdf->setX($x+$this->x_offset);
        return $this;
    }

    public function setOffsetX($x)
    {
        $this->x_offset = $x;
    }

    public function setOffsetY($y)
    {
        $this->y_offset = $y;
    }

    public function font($family, $style='', $size=null)
    {
        // if ($this->current_font == [$family, $style, $size]) return $this;
        $this->pdf->SetFont($family, $style, $size);
        $this->current_font = [$family, $style, $size];
        return $this;
    }
}