<?php namespace Smartsoftware\TcpdfUtils;

use TCPDF;

/**
 * Add table functionality
 *
 * Custom title and columns: fonts, colors, formaters
 */
class Table extends Proxy
{
    public $table_options  = [
        'title'        => [
            'color'      => [215, 215, 215],
            'font'       => ['', '',12],
            'text-color' => [0, 0, 0],
            'height'     => 8,
        ],
        'width'       => 200,
        'lines-color' => [239, 240, 240],
        'font'        => ['','', 10],
        'row-height'  => 8,
        'odd-color'   => [232, 232, 232],
        'text-color'  => [0, 0, 0],
        'border'      => '', //LR
    ];

    /**
     * Set an option
     *
     * @param string $name  Name of option with dot notation title.color
     * @param mixed  $value The value
     */
    public function setTableOption($name, $value)
    {
        array_set($this->table_options, $name, $value);
        return $this;
    }

    /**
     * read table_options
     */
    protected function o($name)
    {
        return array_get($this->table_options, $name);
    }

    /**
     * Draw a table
     *
     * Columns table_options
     *
     * width => 20,       (requerido)
     * title => 'Monto',  (requerido)
     * align => 'L',      (L C R)
     * font  => ['opensans','b',20],
     * text-color  => [0,0,0],
     * renderer => function($row, $pdf) {return 'algo' },
     *
     * @var array
     */
    public function table($data, $columns)
    {
        $o = $this->table_options;

        $pdf = $this->fluent->pdf;

        // Titulos
        call_user_func_array([$pdf,'SetFillColor'], $this->o('title.color'));
        call_user_func_array([$pdf,'SetTextColor'], $this->o('title.text-color'));
        call_user_func_array([$pdf,'SetDrawColor'], $this->o('lines-color'));
        call_user_func_array([$pdf,'SetFont'], $this->o('title.font'));

        $pdf->SetLineWidth(0.3);


        $b = [ 'B' => ['width' => 0.2, 'color' => array(120,120,120), 'dash' => 0, 'cap' => 'butt']];

        foreach ($columns as $column) {
            $align = array_key_exists('align', $column)?$column['align']:'L';
            $pdf->Cell($column['width'], $this->o('title.height'), $column['title'], $b, 0, $align, 1);
        }

        // Cuerpo

        $pdf->Ln();
        // Color and font restoration
        call_user_func_array([$pdf,'SetFillColor'], $o['odd-color']);
        call_user_func_array([$pdf,'SetTextColor'], $o['text-color']);
        // call_user_func_array([$pdf,'SetFont'], $o['font']);

        // Data
        $fill = 0;

        $height = 8;
        $b = $o['border'];

        foreach($data as $row) {
            foreach ($columns as $k => $column) {
                if (array_key_exists('font', $row)) {
                    call_user_func_array([$pdf,'SetFont'], $row['font']);
                } else {
                    // si la columna tiene un font
                    if (array_key_exists('font', $column)) {
                        call_user_func_array([$pdf,'SetFont'], $column['font']);
                    } else {
                        call_user_func_array([$pdf,'SetFont'], $o['font']);
                    }
                }

                $align = array_key_exists('align', $row)?$row['align']:(array_key_exists('align', $column)?$column['align']:'L');
                if (array_key_exists('renderer', $column)) {
                    $f = $column['renderer'];
                    $v = $f($row, $pdf, $k);
                } else {
                    $v = $row[$k];
                }

                $f = array_key_exists('fill', $row)?$row['fill']:$fill;
                $border = array_key_exists('border', $row)?$row['border']:$b;

                $pdf->Cell($column['width'], $o['row-height'], $v, $border, 0, $align, $f);
            }
            $pdf->Ln();
            $fill=!$fill;
        }

        return $this;
    }
}
