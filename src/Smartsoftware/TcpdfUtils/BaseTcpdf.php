<?php namespace Smartsoftware\TcpdfUtils;

use TCPDF;

use Smartsoftware\TcpdfUtils\Traits\CustomHeader;

class BaseTcpdf extends TCPDF
{
    use CustomHeader;
}